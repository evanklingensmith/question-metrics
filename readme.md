## Question Server

### Requirements / Initial Setup

This was tested on Ubunutu 18.04 and requires the following packages/software:

 * python3
 * python3-pip
 * python-virtualenv
 * VS Code
 * nodejs
 * npm

`$ sudo apt-get install python3 python3-pip python3-virtualenv npm nodejs`

Follow instructions for VS Code setup here: https://code.visualstudio.com/docs/setup/linux

Then before setting up the virtual environment:

`$ pip3 install virtualenv`

Setting up the virtual environment

`$ virtualenv venv`

Clone the following repos:

The webserver:

`$ git clone https://gitlab.com/evanklingensmith/question-metrics.git`

The VS Code extension:

`$ git clone https://gitlab.com/evanklingensmith/monitor-extension`

The sample test:

`$ git clone https://gitlab.com/evanklingensmith/python-sample-test`

Move the VS Code extension to the extensions folder:

`mv ./monitor-extension ~/.vscode/extensions/`

Compile the VS Code exension

`.../monitor-extension$ npm install`

`.../monitor-extension$ npm run compile`

### Running the application

Inside the questions metrics folder run the following to activate the python virtual env:

`.../question-metrics$ source ../venv/bin/activate`

Then run the following to install the dependencies:

`(venv).../question-metrics$ pip3 install -r requirements.txt`

Run the following to initialize the database:

`(venv).../question-metrics$ ./manage.py migrate`

Run the following to start the server:

`(venv).../question-metrics$ ./manage.py runserver`

### Notes

Currently, if you would like to delete data it will have to be through the Django provided admin interface at `http://localhost:8000/admin`.  To setup a user for this interface run:

`.../question-metrics$ ./manage.py createsuperuser`

And enter a username and password.  Once logged in, you can remove/alter objects within the database. 

### Walkthrough

 1. Create some students at `localhost:8000/students`
 2. Load in the questions from the question bank at `localhost:8000/questions` (click reload questions)
    * The questsions will load from the sample test repo cloned earlier.
    * This setting can be changed in the django settings file.  The default is `QUESTIONBANK_LOCATION = os.path.abspath(os.path.join(BASE_DIR, '../python-sample-test/'))`
 3. Take a look at the questions and objectives pages, and you will see that there is currently no testing data associated with them
 4. Head over to `localhost:8000/tests` to create a test
 5. Next we should make a test event for the test at `http://localhost:8000/testevents` (hold control to select more than one student)
 6. Hit the start test button to begin the test
 7. When the test event was created, student packages for the test were also generated
    * The default location for these packages is defined in the django settings file: `STUDENT_PACKAGE_DIR = os.path.abspath(os.path.join(BASE_DIR, '../student_package/'))`
    * Navigate there and see that there is now a folder assocaited with the test event id
    * Inside this folder there are individual ziped tar's for each student
    * extract one of them and in this student package folder (while source'd to the python virtual env) run: `python ./run_tests.py`
    * Notice that the student now has data for which questions passed!
    * To easy! Change a question so it fails, or succeeds and run it again, now you will see that the student color is either green or yellow
    * Click on the student to see more detailed information
 8. Now open the student package folder for one of the students in VS Code
    * Start opening files in VS Code in the question directories
    * Notice the log in the terminal that is runnning the development server, there are logs being posted from the student
    * Click on a student in the test event and see that both time spent on a question and the jitter is being tracked
    * Jitter is the ammount of times the student has left that question's files for another question
 9. Once you feel you have enough data, hit the `end test` button
    * See that the students that have passed all questions are marked green, and students that have not passed all questions are marked red
    * Hit the view results button to see in depth analysis of each question and objective for this test
 10. You can now go back to the questions and the objective pages to see that there is now historial data built from the first test.  This will continue to grow as more tests are taken

