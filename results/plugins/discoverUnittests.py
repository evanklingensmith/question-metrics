
import os
import subprocess
import xml.etree.ElementTree as ET
from results.models import Question, UnitTestSuite, TestCase
import unittest, xmlrunner

from django.conf import settings

LOCATION = settings.QUESTIONBANK_LOCATION
TEMP_RESULTS = os.path.join(LOCATION, 'test-reports')

def processTestSuite(suiteXML):
    tree = ET.fromstring(suiteXML)
    suite = tree
    _name = suite.attrib['name'].split('.')[0]
    _total_tests = suite.attrib['tests']
    try:
        _question = Question.objects.get(path_key=_name)
    except KeyError:
        return 0
    defaults = {'total_tests': _total_tests}
    _suite, created = UnitTestSuite.objects.update_or_create(question=_question, name=_name, defaults=defaults)
    for testcase in suite.findall('testcase'):
        _classname = testcase.attrib['classname']
        _name = testcase.attrib['name']
        test_case, created = TestCase.objects.get_or_create(classname=_classname, name=_name, suite=_suite)

def run(): 
    suite = unittest.TestLoader().discover(start_dir=LOCATION, pattern='test_case.py')
    results = xmlrunner.XMLTestRunner(verbosity=2, output=TEMP_RESULTS).run(suite)

    p = subprocess.Popen(str(os.path.join(LOCATION, 'clean.sh')), cwd=LOCATION, shell=True, stdout=subprocess.PIPE)
    p.wait()

    for dirname, dirnames, filenames in os.walk(TEMP_RESULTS):
        for filename in filenames:
            with open(os.path.join(dirname, filename), 'r') as discover:
                processTestSuite(discover.read())
            os.remove(os.path.join(dirname, filename))
    os.rmdir(TEMP_RESULTS)
