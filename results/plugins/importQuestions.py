
import os
import json
import fnmatch
import re
from django.conf import settings
from results.models import Question, UnitTestSuite, TestCase, QuestionFiles, Objective

LOCATION = settings.QUESTIONBANK_LOCATION

def run():
    question = -1
    for root, dirs, files in os.walk(LOCATION):
        if files.__contains__('meta.json'):
            _path_key = root.split('/').pop()
            with open(os.path.join(LOCATION, root, 'meta.json')) as f:
                parsed = json.load(f)
            try:
                _defaults = {
                    'name': parsed['name'],
                    'description': parsed['description'],
                    'language': parsed['lang']
                }
                _objectives = parsed['objectives']
            except KeyError as e:
                _defaults = {
                    'name': 'key error, fix meta.json',
                    'description': 'key error, fix meta.json',
                    'language': 'key error, fix meta.json'
                }
                _objectives = []
            question, created = Question.objects.update_or_create(path_key=_path_key, defaults=_defaults)
            for objective in _objectives:
                try:
                    _defaults = {'description': objective['description']}
                except:
                    _defaults = {'description': 'key error, fix meta.json'}
                objective_obj, created = Objective.objects.update_or_create(name=objective['name'], defaults=_defaults)
                question.objectives.add(objective_obj)
            question.save()
        for file in files:
            if question != -1:
                path = os.path.join(root, file)
                if path.find('.git') > 0 or path.find('.vscode') > 0:
                    pass
                else:
                    file_obj, created = QuestionFiles.objects.update_or_create(path=os.path.join(root, file), question=question)
    
