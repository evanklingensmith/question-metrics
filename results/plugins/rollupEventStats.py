from results.models import (Objective, StudentEventStats, WindowChangeLog, TestEvent, UnitTestSuite, 
                            UnitTestSuiteLog, QuestionTime, QuestionStat, EventQuestionStat, 
                            EventObjectiveStat, TotalQuestionStat, TotalObjectiveStat)

from .updateQuestionStats import StudentStats

class EventStats:
    def __init__(self, event):
        self.event = event

    def update(self):
        for student in self.event.students.all():
            print('updating students stats: {}'.format(student))
            stats = StudentStats(student, self.event)
            stats.update()
        self.gen_question_stats()
        self.update_totals()


    def gen_question_stats(self):
        # create objects for the questions and objectives
        for question in self.event.test.questions.all():
            eqs = EventQuestionStat.objects.get_or_create(question=question, test_event=self.event)
            for objective in question.objectives.all():
                eos, created = EventObjectiveStat.objects.get_or_create(test_event=self.event, objective=objective)
                eos.questions.add(question)                

        # get the student stats for the event
        student_stats = StudentEventStats.objects.filter(test_event=self.event)
        if not student_stats.exists():
            print('student stats do not exist')
            return
        
        objective_map = {}
        objectives_total = []
        for student in self.event.students.all():
            student_stats = StudentEventStats.objects.filter(test_event=self.event, student=student)

            if student_stats.exists():
                stats = student_stats.first()

                obj_met = []
                for qstat in stats.questionstats.all():

                    passed = qstat.last_test_log.passed
                    qtime_try = stats.questiontime.all().filter(question=qstat.question)
                    if qtime_try.exists():
                        qtime = qtime_try.first()
                    else:
                        pass
                    eqs = EventQuestionStat.objects.get(question=qtime.question, test_event=self.event)
                    eqs.total_time += qtime.total_time
                    eqs.total_jitter += qtime.jitter

                    for objective in qstat.question.objectives.all():
                        objectives_total.append(objective.id)
                    
                    if passed:
                        eqs.total_passed += 1
                        for objective in qstat.question.objectives.all():
                            if objective.id not in obj_met:
                                if objective.id in objective_map:
                                    objective_map[objective.id] += 1
                                else:
                                    objective_map[objective.id] = 1
                                obj_met.append(objective.id)
                    eqs.save(update_fields=["total_time", "total_jitter", "total_passed"])

        for objective in set(objectives_total):
            obj = Objective.objects.get(id=objective)
            count = 0
            try:
                count = objective_map[objective]
                if (count > len(self.event.students.all())):
                    count = len(self.event.students.all())
            except:
                pass
            eos = EventObjectiveStat.objects.get(test_event=self.event, objective=obj)
            eos.total_complete = count
            eos.save(update_fields=['total_complete'])

            
    
    def update_totals(self):
        student_count = len(self.event.students.all())
        for eqs in EventQuestionStat.objects.filter(test_event=self.event):
            tqs, created = TotalQuestionStat.objects.get_or_create(question=eqs.question)
            tqs.times_attempted += int(student_count)
            tqs.times_passed += eqs.total_passed
            tqs.total_jitter += eqs.total_jitter
            tqs.total_time += eqs.total_time
            tqs.save(update_fields=["total_time", "total_jitter", "times_passed", "times_attempted"])

        for eos in EventObjectiveStat.objects.filter(test_event=self.event):
            tos, created = TotalObjectiveStat.objects.get_or_create(objective=eos.objective)
            tos.times_attempted += int(student_count)
            tos.times_completed += eos.total_complete
            tos.save(update_fields=["times_completed", "times_attempted"])







