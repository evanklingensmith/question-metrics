
from results.models import (StudentEventStats, WindowChangeLog, TestEvent, UnitTestSuite, 
                            UnitTestSuiteLog, QuestionTime, QuestionStat)

import datetime
from django.utils import timezone
import pytz

class StudentStats:
    def __init__(self, student, event):
        self.student = student
        self.event = event
        test = StudentEventStats.objects.filter(student=self.student, test_event=self.event)
        if test.exists():
            self.stat = test.first()
        elif WindowChangeLog.objects.filter(test_event=event).filter(student=student).exists():
            first_log = WindowChangeLog.objects.filter(test_event=event).filter(student=student).first()
            self.stat = StudentEventStats.objects.create(student=student, test_event=event, last_window_log=first_log)
        else:
            timezone.now()
            timestamp = pytz.utc.localize(datetime.datetime.now())
            first_log = WindowChangeLog.objects.create(test_event=event, student=student, timestamp=timestamp, filename='meta')
            self.stat = StudentEventStats.objects.create(student=student, test_event=event, last_window_log=first_log)
        return

    def __str__(self):
        return '{}: {}: {}'.format(str(self.id), self.timestamp, self.filename)

    def update(self):
        self.process_tests()
        self.process_time()

    def process_tests(self):
        if self.stat == None:
            return
        suite_logs = UnitTestSuiteLog.objects.filter(test_event=self.event).filter(student=self.student)
        # check there acutally has been some suite logs
        questions_passed = 0
        if suite_logs.exists():
            objectives_met = []
            for question in self.event.test.questions.all():
                # grab the last suite, set it and see if it passed
                question_suite = UnitTestSuite.objects.filter(question=question).first()
                suite = suite_logs.filter(suite=question_suite).order_by('-id')
                if suite.exists():
                    suite = suite.first()
                    if suite.passed:
                        questions_passed += 1
                        for objective in question.objectives.all():
                            objectives_met.append(objective.id)

                    qstat = QuestionStat.objects.filter(student_stats=self.stat, question=question)
                    if qstat.exists() and qstat.first().last_test_log.id is not suite.id:
                        update_me = qstat.first()
                        update_me.last_test_log = suite
                        update_me.save(update_fields=["last_test_log"])
                    elif not qstat.exists():
                        qstat = QuestionStat.objects.create(student_stats=self.stat, question=question, last_test_log=suite)
            self.stat.objectives_passed = len(set(objectives_met))
            self.stat.questions_passed = questions_passed
            self.stat.save(update_fields=["questions_passed", "objectives_passed"])
        else:
            return
        print('done process tests: {}'.format(self.student))

    def process_time(self):
        if self.stat == None:
            return

        for question in self.event.test.questions.all():
            try_time = QuestionTime.objects.filter(student_stats=self.stat, question=question)
            if not try_time.exists():
                qtime = QuestionTime.objects.create(student_stats=self.stat, question=question, total_time=datetime.timedelta())


        # get all the window logs that have not been processed yet
        last_id = self.stat.last_window_log.id
        window_logs = WindowChangeLog.objects.filter(test_event=self.event).filter(student=self.student).filter(id__gte=last_id)

        # create a key map for the question path keys
        question_map = {}
        for question in self.event.test.questions.all():
            question_map[question.path_key] = question
        
        # pair log entries with questions
        process_log = []
        for log in window_logs:
            print (log)
            for key in question_map.keys():
                if str(log.filename).find(key) > 0:
                    process_log.append({
                        'question': question_map[key],
                        'timestamp': log.timestamp,
                        'log ID': log.id,
                    })

        if len(process_log) > 0:
            start_log = process_log.pop()
            process_log.reverse()
            for log in process_log:
                print (log)
                next_log = log
                question = start_log['question']
                time_diff = start_log['timestamp'] - next_log['timestamp'] 
                print('add: {} to {}'.format(time_diff, question))
                qtime = QuestionTime.objects.filter(student_stats=self.stat, question=question)
                if qtime.exists():
                    update_me = qtime.first()
                    prev_time = update_me.total_time
                    if(start_log['question'].id is not next_log['question'].id):
                        update_me.jitter += 1
                    update_me.total_time = prev_time + time_diff
                    update_me.save(update_fields=["total_time", "jitter"])
                else:
                    qtime = QuestionTime.objects.create(student_stats=self.stat, question=question, total_time=time_diff)
                    qtime.save()
                start_log = next_log

        
        self.stat.last_window_log = WindowChangeLog.objects.filter(test_event=self.event).filter(student=self.student).last()
        self.stat.save(update_fields=["last_window_log"])
            
