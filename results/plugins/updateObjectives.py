import os
import json
from results.models import Question, Objective


LOCATION = '/home/evan/gatech/questionbank/'

def addOrUpdateObjective(objective):
    for question in objective.questions.all():
        meta_location = question.path_key
        meta_location = os.path.join(LOCATION, question.path_key, 'meta.json')
        with open(meta_location) as f:
            parsed = json.load(f)
            try:
                _objectives = parsed['objectives']
            except KeyError as error:
                print(error)
                return -1
            for obj in _objectives:
                if objective.name == obj['name']:
                    parsed['objectives'].remove(obj)
            parsed['objectives'].append({
                'name': objective.name,
                'description': objective.description
            })
            new_meta = json.dumps(parsed)
            f.close()
        if len(new_meta) > 0:
            os.remove(meta_location)
            with open(meta_location, "w+") as f:
                f.write(new_meta)
                f.close()
    return 1

def deleteObjetive(objective):
    for question in objective.questions.all():
        meta_location = question.path_key
        meta_location = os.path.join(LOCATION, question.path_key, 'meta.json')
        with open(meta_location) as f:
            parsed = json.load(f)
            try:
                _objectives = parsed['objectives']
            except KeyError as error:
                print(error)
                return -1
            for obj in _objectives:
                if objective.name == obj['name']:
                    parsed['objectives'].remove(obj)
            new_meta = json.dumps(parsed)
            f.close()
        if len(new_meta) > 0:
            os.remove(meta_location)
            with open(meta_location, "w+") as f:
                f.write(new_meta)
                f.close()
    return 1