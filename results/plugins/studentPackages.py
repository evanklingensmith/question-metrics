
import os
import tarfile
import string
import random
from django.conf import settings
from results.models import Question, Test, QuestionFiles, TestEvent, StudentPackage

def create(test_event):
    test = test_event.test
    questions = test.questions.all()
    event_dir = os.path.join(settings.STUDENT_PACKAGE_DIR, str(test_event.id))
    if not os.path.exists(event_dir):
        os.makedirs(event_dir)

    for student in test_event.students.all():
        package_name = str(student.id) + '.tar.gz'
        package_path = os.path.join(event_dir, package_name)
        with tarfile.open(package_path, 'w:gz') as tar_handle:
            for question in questions:
                for file in question.files.all():
                    path = file.path
                    if path.find('/solution/') < 0 and path.find('meta.json') and path.find('.vscode') < 0:
                        arcname=path.replace(settings.QUESTIONBANK_LOCATION, '')
                        tar_handle.add(path, arcname=arcname)
            tar_handle.add(os.path.join(settings.QUESTIONBANK_LOCATION, 'run_tests.py'), arcname='run_tests.py')

            # create config file for unit tests
            key = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.ascii_lowercase+ string.digits) for _ in range(50))
            exists = os.path.isfile(os.path.join(settings.STUDENT_PACKAGE_DIR, 'conf.py'))
            if exists:
                os.remove(os.path.join(settings.STUDENT_PACKAGE_DIR, 'conf.py'))
            with open(os.path.join(settings.STUDENT_PACKAGE_DIR, 'conf.py'), 'w') as f:
                f.write('SERVER = \'localhost:8000\'\n')
                f.write('STUDENT_ID = \'{}\'\n'.format(student.id))
                f.write('EVENT_ID = \'{}\'\n'.format(test_event.id))
                f.write('KEY = \'{}\'\n'.format(key))
            tar_handle.add(os.path.join(settings.STUDENT_PACKAGE_DIR, 'conf.py'), arcname='conf.py')
            os.remove(os.path.join(settings.STUDENT_PACKAGE_DIR, 'conf.py'))

            # create vs code extension config file
            exists = os.path.isfile(os.path.join(settings.STUDENT_PACKAGE_DIR, 'settings.json'))
            if exists:
                os.remove(os.path.join(settings.STUDENT_PACKAGE_DIR, 'settings.json'))
            with open(os.path.join(settings.STUDENT_PACKAGE_DIR, 'settings.json'), 'w') as f:
                f.write('{{\n"windowlog.student_id": {},\n'.format(student.id))
                f.write('"windowlog.event_id": {},\n'.format(test_event.id))
                f.write('"windowlog.server": "{}",\n'.format('localhost:8000'))
                f.write('"windowlog.key": "{}"\n}}'.format(key))
            tar_handle.add(os.path.join(settings.STUDENT_PACKAGE_DIR, 'settings.json'), arcname='.vscode/settings.json')
            os.remove(os.path.join(settings.STUDENT_PACKAGE_DIR, 'settings.json'))
            
        package = StudentPackage.objects.get_or_create(path=package_path, key=key, test_event=test_event, student=student)



   






    
