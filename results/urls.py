from django.urls import path

from . import views

urlpatterns = [
    path('', views.testEvents, name='index'),
    path('<int:event_id>/', views.testEvent, name='testevent'),
    path('<int:event_id>/results', views.testEventResults, name='testeventresults'),
    path('<int:event_id>/<int:student_id>/addwindowlog/', views.addwindowlog, name='window'),
    path('<int:event_id>/<int:student_id>/addunittestlog/', views.addunittestlog, name='unittest'),
    path('<int:event_id>/<int:student_id>', views.testEventStudent, name='testeventstudent'),
    path('students', views.students, name='students'),
    path('addstudent', views.addStudent, name='addstudent'),
    path('loadquestions', views.loadquestions, name='loadquestions'),
    path('tests', views.tests, name='tests'),
    path('createtest', views.createTest, name='createtest'),
    path('createtestevent', views.createTestEvent, name='createtestevent'),
    path('testevents', views.testEvents, name='testevents'),
    path('createobjective', views.createObjective, name='createobjective'),
    path('objectives', views.objectives, name='objectives'),
    path('questions', views.questions, name='questions'),
]   