from django.contrib import admin
from .models import Question, QuestionFiles, Student, TestEvent, TestCase, UnitTestSuite, UnitTestSuiteLog, TestCaseLog, Test, StudentEventStats
from .models import QuestionTime, QuestionStat
from .models import TotalQuestionStat, EventObjectiveStat, EventQuestionStat, TotalObjectiveStat
# Register your models here.

@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    pass

@admin.register(QuestionFiles)
class QuestionFilesAdmin(admin.ModelAdmin):
    pass

@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    pass

@admin.register(TestEvent)
class TestEventAdmin(admin.ModelAdmin):
    pass

@admin.register(TestCase)
class TestCaseAdmin(admin.ModelAdmin):
    pass

@admin.register(UnitTestSuite)
class UnitTestSuiteAdmin(admin.ModelAdmin):
    pass

@admin.register(UnitTestSuiteLog)
class UnitTestSuiteLogAdmin(admin.ModelAdmin):
    pass

@admin.register(TestCaseLog)
class TestCaseLogAdmin(admin.ModelAdmin):
    pass

@admin.register(Test)
class TestAdmin(admin.ModelAdmin):
    pass

@admin.register(StudentEventStats)
class StudentEventStats(admin.ModelAdmin):
    pass

@admin.register(QuestionStat)
class QuestionStat(admin.ModelAdmin):
    pass

@admin.register(QuestionTime)
class QuestionTime(admin.ModelAdmin):
    pass

@admin.register(EventQuestionStat)
class EventQuestionStat(admin.ModelAdmin):
    pass

@admin.register(EventObjectiveStat)
class EventObjectiveStat(admin.ModelAdmin):
    pass

@admin.register(TotalQuestionStat)
class TotalQuestionStat(admin.ModelAdmin):
    pass

@admin.register(TotalObjectiveStat)
class TotalObjectiveStat(admin.ModelAdmin):
    pass