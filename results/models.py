from django.db import models
import datetime


class Objective(models.Model):
    name = models.CharField(max_length=50, unique=True)
    description = models.CharField(max_length=2000)

class Question(models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=2000)
    # TODO: enums for langauge
    language = models.CharField(max_length=20)
#    search_tags = ManyToManyField(SearchTag)
    objectives = models.ManyToManyField(Objective, related_name='questions')
    path_key = models.CharField(max_length=50, unique=True)

    def __unicode__(self):
        return '%s' % self.name
    
    def __str__(self):
        return self.name

class Test(models.Model):
    name = models.CharField(max_length=50, unique=True)
    description = models.CharField(max_length=2000)
    questions = models.ManyToManyField(Question, related_name='tests')

    def __str__(self):
        return self.name

class TestEvent(models.Model):
    name = models.CharField(max_length=50)
    started = models.BooleanField(default=False)
    ended = models.BooleanField(default=False)
    start_time = models.DateTimeField(auto_now_add=True, blank=True)
    end_time = models.DateTimeField(auto_now_add=True, blank=True)
    test = models.ForeignKey(Test, related_name='test_events', on_delete=models.CASCADE)

    class Meta:
        ordering = ['-id']
    
    def duration(self):
        if not self.started:
            return 'Not Started'
        if self.ended:
            time_total = self.end_time - self.start_time
            hours, remainder = divmod(time_total.total_seconds(), 3600)
            minutes, seconds = divmod(remainder, 60)
            if hours > 0:
                time = '{} hr {} min'.format(int(hours), int(minutes))
            else:
                time = '{} min'.format(int(minutes))
            return time
        return 'Test Ongoing'
        


class QuestionFiles(models.Model):
    question = models.ForeignKey(Question, related_name='files', on_delete=models.CASCADE)
    path = models.CharField(max_length=2000)

class SearchTag(models.Model):
    tag = models.CharField(max_length=50)

class Student(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    tests = models.ManyToManyField(TestEvent, related_name='students')

    def __str__(self):
        return str(self.id) + ': ' + self.first_name + ' ' + self.last_name

class StudentPackage(models.Model):
    path = models.CharField(max_length=2000)
    key = models.CharField(max_length=50)
    test_event = models.ForeignKey(TestEvent, related_name='packages', on_delete=models.CASCADE)
    student = models.ForeignKey(Student, related_name='packages', on_delete=models.CASCADE)

class UnitTestSuite(models.Model):
    question = models.ForeignKey(Question, related_name='suites', on_delete=models.CASCADE)
    total_tests = models.PositiveIntegerField(default=0)
    name = models.CharField(max_length=256)

class UnitTestSuiteLog(models.Model):
    suite = models.ForeignKey(UnitTestSuite, related_name='log_entries', on_delete=models.CASCADE)

    # store the specific test event and student ID
    test_event = models.ForeignKey(TestEvent, on_delete=models.CASCADE)
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    passed = models.BooleanField(default=False)

    # information on the tests run
    passed_sum = models.PositiveIntegerField(default=0)
    tests_run = models.PositiveIntegerField(default=0)
    errors = models.PositiveIntegerField(default=0)
    failures = models.PositiveIntegerField(default=0)
    skipped = models.PositiveIntegerField(default=0)
    time = models.DecimalField(default=0.0, decimal_places=6, max_digits=15)
    timestamp = models.DateTimeField('time run')

    def __str__(self):
        return '{} : {} passed: {} / {}'.format(self.id, self.suite.name, str(self.passed_sum), str(self.tests_run))

class TestCase(models.Model):
    classname = models.CharField(max_length=256)
    name = models.CharField(max_length=256)
    suite = models.ForeignKey(UnitTestSuite, related_name='testcases', on_delete=models.CASCADE)

class TestCaseLog(models.Model):
    test_case = models.ForeignKey(TestCase, related_name='log_entries', on_delete=models.CASCADE)
    suite_log = models.ForeignKey(UnitTestSuiteLog, related_name='testcases_log', on_delete=models.CASCADE)
    passed = models.BooleanField(default=False)
    message = models.CharField(max_length=5000)
    timestamp = models.DateTimeField('time run')

class WindowChangeLog(models.Model):
    test_event = models.ForeignKey(TestEvent, related_name='windowlog', on_delete=models.CASCADE)
    timestamp = models.DateTimeField('time switched')
    filename = models.CharField(max_length=256)
    student = models.ForeignKey(Student, on_delete=models.CASCADE)

    def __str__(self):
        return '{}: {}: {}'.format(str(self.id), self.timestamp, self.filename)

class StudentEventStats(models.Model):
    test_event = models.ForeignKey(TestEvent, related_name='eventstats', on_delete=models.CASCADE)
    student = models.ForeignKey(Student, related_name='eventstats', on_delete=models.CASCADE)
    questions_passed = models.PositiveIntegerField(default=0)
    objectives_passed = models.PositiveIntegerField(default=0)
    last_window_log = models.ForeignKey(WindowChangeLog, on_delete=models.CASCADE)

class QuestionStat(models.Model):
    student_stats = models.ForeignKey(StudentEventStats, related_name='questionstats', on_delete=models.CASCADE)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    last_test_log = models.ForeignKey(UnitTestSuiteLog, on_delete=models.CASCADE)

class QuestionTime(models.Model):
    student_stats = models.ForeignKey(StudentEventStats, related_name='questiontime', on_delete=models.CASCADE)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    total_time = models.DurationField(default=datetime.timedelta())
    jitter = models.PositiveIntegerField(default=0)

class EventQuestionStat(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    test_event = models.ForeignKey(TestEvent, related_name='eventquestionstats', on_delete=models.CASCADE)

    # get the total time for the question in the testing period
    total_time = models.DurationField(default=datetime.timedelta())
    total_jitter = models.PositiveIntegerField(default=0)
    total_passed = models.PositiveIntegerField(default=0)

    def avg_completion(self):
        return float(self.total_passed/len(self.test_event.students.all()))*100
    
    def avg_time(self):
        avg = self.total_time / len(self.test_event.students.all())

        if avg > datetime.timedelta(0):
            hours, remainder = divmod(avg.total_seconds(), 3600)
            minutes, seconds = divmod(remainder, 60)
            if hours > 0:
                time = '{} hr {} min'.format(int(hours), int(minutes))
            else:
                time = '{} min'.format(int(minutes))
        else:
            time = 'Never Completed'
        
        return time

    def portion_time(self):
        avg = self.total_time / len(self.test_event.students.all())
        start_time = self.test_event.start_time
        end_time = self.test_event.end_time
        event_time = end_time - start_time

        
        return(int((avg.total_seconds() / event_time.total_seconds()) * 100))


    def avg_jitter(self):
        return int(self.total_jitter/len(self.test_event.students.all()))
    
    def jitter_bar(self):
        my_jitter = self.total_jitter
        big_jitter = 0
        
        for question in self.test_event.eventquestionstats.all():
            if question.total_jitter > big_jitter:
                big_jitter = question.total_jitter
        if big_jitter:
            return (my_jitter / big_jitter) * 100
        else:
            return 0

class EventObjectiveStat(models.Model):
    objective = models.ForeignKey(Objective, on_delete=models.CASCADE)
    test_event = models.ForeignKey(TestEvent, related_name='eventobjstats', on_delete=models.CASCADE)
    total_complete = models.PositiveIntegerField(default=0)
    questions = models.ManyToManyField(Question)

    def avg(self):
        return(float(self.total_complete / len(self.test_event.students.all()))*100)

class TotalQuestionStat(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    times_attempted = models.PositiveIntegerField(default=0)
    times_passed = models.PositiveIntegerField(default=0)
    total_time = models.DurationField(default=datetime.timedelta())
    total_jitter = models.PositiveIntegerField(default=0)

    def avg_completion(self):
        return float(self.times_passed/self.times_attempted)*100
    
    def avg_time(self):
        avg = self.total_time / self.times_attempted
        if avg > datetime.timedelta(0):
            hours, remainder = divmod(avg.total_seconds(), 3600)
            minutes, seconds = divmod(remainder, 60)
            if hours > 0:
                time = '{} hr {} min'.format(int(hours), int(minutes))
            else:
                time = '{} min'.format(int(minutes))
        else:
            time = 'Never Completed'
        
        return time

    def avg_jitter(self):
        return int(self.total_jitter/self.times_attempted)
    
    def difficulty(self):
        avg = float(self.times_passed/self.times_attempted)*100
        if avg > 90:
            return 'Easy'
        if avg > 70:
            return 'Medium'
        return 'Hard'
    

class TotalObjectiveStat(models.Model):
    objective = models.ForeignKey(Objective, on_delete=models.CASCADE)
    times_attempted = models.PositiveIntegerField(default=0)
    times_completed = models.PositiveIntegerField(default=0)

    def avg(self):
        return(float(self.times_completed / self.times_attempted)*100)

