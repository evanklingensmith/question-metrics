from datetime import datetime, timedelta
from .plugins import importQuestions, discoverUnittests
from .plugins.updateQuestionStats import StudentStats
from .plugins.rollupEventStats import EventStats
from django.shortcuts import render, redirect, reverse
from django.http import HttpResponse, HttpResponseRedirect
from .models import Student, WindowChangeLog, UnitTestSuite, TestCase 
from .models import TestEvent, Question, Test, UnitTestSuiteLog, TestCaseLog 
from .models import Objective, StudentPackage, StudentEventStats
from .models import QuestionStat, QuestionTime
from .models import EventQuestionStat, EventObjectiveStat, TotalQuestionStat, TotalObjectiveStat
from .forms import StudentForm, TestForm, TestEventForm, ObjectiveForm
import xml.etree.ElementTree as ET
from django.utils import timezone
import pytz


def index(request):
    return HttpResponse("Hello, world. You're at the index.")

def createTest(request):
    if request.method == 'POST':
        form = TestForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('tests'))
    else:
        return HttpResponse('fail')

def createTestEvent(request):
    if request.method == 'POST':
        try:
            form = TestEventForm(request.POST)
        except:
            return HttpResponse('Parameters Invalid')

        if form.is_valid():
            try:
                instance = form.save()
            except:
                return HttpResponse('Parameters Invalid')
            return HttpResponseRedirect(reverse('testevents'))
    else:
        return HttpResponse('fail')

def createObjective(request):
    if request.method == 'POST':
        form = ObjectiveForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('objectives'))
    else:
        return HttpResponse('fail')

def objectives(request):
    form = ObjectiveForm()
    objectives = Objective.objects.all()

    os = []
    for objective in objectives:
        tos = TotalObjectiveStat.objects.filter(objective=objective)
        tests = []
        for question in objective.questions.all():
            for test in question.tests.all():
                tests.append(test)
        if tos.exists():
            os.append({
                'objective': objective,
                'total': tos.first(),
                'tests': set(tests)
            })
        else:
            os.append({
                'objective': objective,
                'tests': set(tests),
                'nototal': True
            })
        
    context = {
        'objectiveform': form,
        'os': os,
    }
    return render(request, 'objectives.html', context)

def questions(request):
    questions = Question.objects.all()

    qs = []
    for question in questions:
        tqs = TotalQuestionStat.objects.filter(question=question)
        if tqs.exists():
            qs.append({
                'question': question,
                'total': tqs.first()
            })
        else:
            qs.append({
                'question': question,
                'nototal': True
            })
        

    context = {
        'qs': qs,
    }
    return render(request, 'questions.html', context)


def testEvents(request):
    initial = {
        'start_time': datetime.now(),
        'end_time': datetime.now() + timedelta(days=1)
    }
    create_test_form = TestEventForm(initial=initial)
    test_events = TestEvent.objects.all()
    context = {
        'testevents': test_events,
        'create_test_form': create_test_form,
    }
    return render(request, 'testevents.html', context)

def testEventStudent(request, event_id, student_id):
    try:
        event = TestEvent.objects.get(id=event_id)
        student = Student.objects.get(id=student_id)
        package = StudentPackage.objects.filter(test_event=event, student=student).first()
    except:
        return HttpResponse('Test event and student with those id do not exist')

    stats = StudentStats(student, event)
    stats.update()
    event_stats = 'None'

    totaltime = 'Not started'
    if stats.stat is not None:
        event_stats = StudentEventStats.objects.filter(test_event=event, student=student).first()
        totaltime = 'Ongoing'
        if event_stats.questions_passed == len(event.test.questions.all()):
            start = UnitTestSuiteLog.objects.filter(student=student).order_by('id')
            if start.exists():
                start_time = event.start_time
                end_time = start.last().timestamp
                time_total = end_time - start_time
                hours, remainder = divmod(time_total.total_seconds(), 3600)
                minutes, seconds = divmod(remainder, 60)
                if hours > 0:
                    totaltime = '{} hr {} min'.format(int(hours), int(minutes))
                else:
                    totaltime = '{} min'.format(int(minutes))
        if event.ended and event_stats.questions_passed is not len(event.test.questions.all()):
            totaltime = 'Did not finish'
    
    objectives_all = []
    for question in event.test.questions.all():
        for objective in question.objectives.all():
            objectives_all.append(objective.id)

    questions = []
    for question in event.test.questions.all():
        if stats.stat is not None:
            test = QuestionStat.objects.filter(student_stats=event_stats, question=question).first()
            time = QuestionTime.objects.filter(student_stats=event_stats, question=question).first()
        else:
            test = 'none'
            time = 'none'
        questions.append({
            'question': question,
            'test': test,
            'time': time,
        })




    context = {
        'event': event,
        'totaltime': totaltime,
        'student': student,
        'package': package,
        'eventstats': event_stats,
        'questions': questions,
        'question_sum': len(event.test.questions.all()),
        'objective_sum': len(set(objectives_all))
    }
    return render(request, 'testeventstudent.html', context)



def tests(request):
    form = TestForm()
    tests = Test.objects.all()

    ts = []

    for test in tests:
        objs = []
        for question in test.questions.all():
            for objective in question.objectives.all():
                objs.append(objective)
        ts.append({
            'test': test,
            'objs': set(objs)
        })

    context = {
        'ts': ts,
        'form': form
    }
    return render(request, 'tests.html', context)

def testEventResults(request, event_id):
    try:
        event = TestEvent.objects.get(id=event_id)
    except:
        return HttpResponse('Test event with that id does not exist')

    if request.method == 'POST':
        try:
            if request.POST['action'] == 'tryreload':
                if event.ended:
                    EventQuestionStat.objects.filter(test_event=event).delete()
                    EventObjectiveStat.objects.filter(test_event=event).delete()
                    stats = EventStats(event)
                    stats.update()
                    redirect('testevent', event_id=event_id)
        except:
             redirect('testevent', event_id=event_id)

    total_students = len(event.students.all())
    if total_students < 1:
        return HttpResponse('Something went wrong, no students for event')
    qs = []
    total_questions_passed = 0
    total_questions = 0
    for question in EventQuestionStat.objects.filter(test_event=event):
        qs.append({
            'question': question,
            'total': TotalQuestionStat.objects.filter(question=question.question).first()
        })
        total_questions_passed += question.total_passed 
        total_questions += 1
    
    questions_bar = 0
    if(total_questions):
        questions_bar = float(total_questions_passed / (total_students*total_questions)) *100

    os = []
    total_obj_passed = 0
    total_obj = 0
    for objective in EventObjectiveStat.objects.filter(test_event=event):
        os.append({
            'objective': objective,
            'total': TotalObjectiveStat.objects.filter(objective=objective.objective).first()
        })
        total_obj_passed += objective.total_complete
        total_obj +=1
    
    obj_bar = 0
    if (total_obj):
        obj_bar = (total_obj_passed / (total_students*total_obj)) * 100
    

    suite_logs = UnitTestSuiteLog.objects.filter(test_event=event)
    window_logs = WindowChangeLog.objects.filter(test_event=event)

    passed_total = 0
    completed = []
    question_sum = len(event.test.questions.all())
    for student in event.students.all():
        questions_passed = 0
        objectives_met = []
        for question in event.test.questions.all():
            suite = UnitTestSuite.objects.filter(question=question).first()
            unit_tests = suite_logs.filter(student=student).filter(suite=suite).order_by('-id')

            if len(suite_logs.filter(student=student).filter(suite=suite).order_by('-id')) > 0:
                passed = True
                for test in unit_tests.first().testcases_log.all():
                    if test.passed == False:
                        passed = False
                if passed:
                    questions_passed += 1

        if(questions_passed == total_questions):
            passed_total += 1
            completed.append(student)

    total_time = timedelta(0)
    if len(completed) > 0:
        for student in completed:
            end = UnitTestSuiteLog.objects.filter(test_event=event, student=student).last().timestamp
            start = event.start_time
            total_time += (end - start)
        
        avg_time = total_time / len(completed)

        hours, remainder = divmod(avg_time.total_seconds(), 3600)
        minutes, seconds = divmod(remainder, 60)
        if hours > 0:
            avg_completion = '{} hr {} min'.format(int(hours), int(minutes))
        else:
            avg_completion = '{} min'.format(int(minutes))
    else:
        avg_completion = 'None Completed'
    
    context = {
        'qs': qs,
        'os': os,
        'event': event,
        'total_students': total_students,
        'total_questions': total_questions,
        'questions_bar': questions_bar,
        'total_obj': total_obj,
        'obj_bar': obj_bar,
        'passed': passed_total,
        'avg_completion': avg_completion,
    }
    return render(request, 'testeventresults.html', context)

    

def testEvent(request, event_id):
    try:
        event = TestEvent.objects.get(id=event_id)
    except:
        return HttpResponse('Test event with that id does not exist')
    if request.method == 'POST':
        try:
            timezone.now()
            if request.POST['action'] == 'startevent':
                if event.ended != True:
                    event.start_time = pytz.utc.localize(datetime.now())
                    event.started = True
                    event.save(update_fields=['start_time', 'started'])
                    redirect('testevent', event_id=event_id)
            else:
                if event.ended != True:
                    event.end_time = pytz.utc.localize(datetime.now())
                    event.ended = True
                    event.save(update_fields=['end_time', 'ended'])
                    stats = EventStats(event)
                    stats.update()
                    redirect('testevent', event_id=event_id)
        except:
             redirect('testevent', event_id=event_id)


    suite_logs = UnitTestSuiteLog.objects.filter(test_event=event)
    window_logs = WindowChangeLog.objects.filter(test_event=event)
    students = []
    question_sum = len(event.test.questions.all())
    for student in event.students.all():
        questions_passed = 0
        objectives_met = []
        for question in event.test.questions.all():
            suite = UnitTestSuite.objects.filter(question=question).first()
            unit_tests = suite_logs.filter(student=student).filter(suite=suite).order_by('-id')

            if len(suite_logs.filter(student=student).filter(suite=suite).order_by('-id')) > 0:
                passed = True
                for test in unit_tests.first().testcases_log.all():
                    if test.passed == False:
                        passed = False
                if passed:
                    questions_passed += 1
                    for objective in question.objectives.all():
                        objectives_met.append(objective.id)
        time = 'Not started'
        if window_logs.filter(student=student).exists() or suite_logs.filter(student=student).exists():
            time = 'Ongoing'
        if questions_passed == question_sum:
            start = suite_logs.filter(student=student).order_by('id')
            if len(start) > 0:
                start_time = event.start_time
                end_time = start.last().timestamp
                time_total = end_time - start_time
                hours, remainder = divmod(time_total.total_seconds(), 3600)
                minutes, seconds = divmod(remainder, 60)
                if hours > 0:
                    time = '{} hr {} min'.format(int(hours), int(minutes))
                else:
                    time = '{} min'.format(int(minutes))
        if event.ended and questions_passed != question_sum:
            time = 'Did not finish'
        warn = True
        if questions_passed == question_sum:
            warn = False

        
        students.append({
            'object': student,
            'totaltime': time,
            'passed': questions_passed,
            'objectives': len(set(objectives_met)),
            'warn': warn
        })
    objectives_all = []
    for question in event.test.questions.all():
        for objective in question.objectives.all():
            objectives_all.append(objective)
    


    context = {'event': event,
            'students': students,
            'suite_logs': suite_logs,
            'window_logs': window_logs,
            'question_sum': question_sum,
            'objective_sum': len(set(objectives_all)),
            'objs': objectives_all,
            }
    return render(request, 'testevent.html', context)

def addStudent(request):
    if request.method == 'POST':
        form = StudentForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('students'))
    else:
        return HttpResponse('fail')

def students(request):
    _students = Student.objects.all()
    form = StudentForm()
    return render(request, 'students.html', {'form': form, 'students': _students})

def loadquestions(request):
    context = {'questions': Question.objects.all()}
    if request.method == 'POST':
        importQuestions.run()
        discoverUnittests.run()
        return HttpResponseRedirect(reverse('questions'))
    return HttpResponseRedirect(reverse('questions'))

def addwindowlog(request, student_id, event_id):
    try:
        _student = Student.objects.get(id=student_id)
        _event = TestEvent.objects.get(id=event_id)
        package = StudentPackage.objects.filter(test_event=_event, student=_student).first()
    except:
        print('window post failed')
        _student = Student()
        return HttpResponse("student or event does not exist")
    if _event.started == True and _event.ended == False:
        if request.method == 'POST':
            if request.POST['key'] != package.key:
                print("key error on POST for window log")
                return HttpResponse("student or event does not exist")
            try:
                _filename = request.POST['filename']
                timezone.now()
                # Sun Oct 28 2018 20:11:59 GMT-0500 (CDT)    
                #             print('here')
                _timestamp = datetime.utcfromtimestamp(int(request.POST['timestamp']))
                _timestamp = pytz.utc.localize(_timestamp)
                if _filename and _timestamp:
                    window_change = WindowChangeLog(
                        timestamp=_timestamp, filename=_filename, student=_student, test_event=_event)
                    window_change.save()
                    print('Adding window log sid: {} file: {} time: {}'.format(student.id, _filename, _timestamp))
                return HttpResponse("success")
            except:
                return HttpResponse('failure')
    window_log = WindowChangeLog.objects.filter(student=_student)
    context = {'window_log': window_log}
    return render(request, 'windowlog.html', context)


def addunittestlog(request, student_id, event_id):
    timezone.now()
    try:
        _student = Student.objects.get(id=student_id)
        _test_event = TestEvent.objects.get(id=event_id)
    except:
        return HttpResponse('student or test event does not exist')
    if _test_event.started and not _test_event.ended:
        if request.method == 'POST':
            tree = ET.fromstring(request.body)
            suite = tree   
            try:
                _suite = UnitTestSuite.objects.get(name=suite.attrib['name'].split('.')[0])
                package = StudentPackage.objects.filter(test_event=_test_event, student=_student).first()
            except KeyError as e:
                return HttpResponse('failure: {}'.format(e))
            if request.META['HTTP_KEY'] != package.key:
                print("key error on POST for unit test")
                return HttpResponse('failure')
            _tests = suite.attrib['tests']
            if(int(_tests) != int(_suite.total_tests)):
                print('the ammount of tests has changed {}, {}'.format(_tests, _suite.total_tests))
            _errors = suite.attrib['errors']
            _failures = suite.attrib['failures']
            # _skipped = suite.attrib['skipped']
            _skipped = 0
            _time = suite.attrib['time']
            # _timestamp = pytz.utc.localize(datetime.strptime(suite.attrib['timestamp'], '%Y-%m-%dT%H:%M:%S'))
            _timestamp = pytz.utc.localize(datetime.now())
            suite_passed = True
            suite_sum_passed = 0
            _log_suite = UnitTestSuiteLog(student=_student, test_event=_test_event, suite=_suite, errors=_errors, failures=_failures,
                    skipped=_skipped, time=_time, timestamp=_timestamp, passed=suite_passed)
            _log_suite.save()
            tests_run = 0
            for testcase in suite.findall('testcase'):
                try:
                    tests_run += 1
                    _test_case = TestCase.objects.get(name=testcase.attrib['name'])
                    _timestamp = pytz.utc.localize(datetime.now())
                    _passed = True
                    for message in testcase:
                        if message.tag == 'error':
                            _message = message.attrib['message']
                            _passed = False
                            suite_passed = False
                    if _passed:
                        suite_sum_passed += 1
                        _message = ''
                    test_case = TestCaseLog(test_case=_test_case, timestamp=_timestamp, passed=_passed, message=_message, suite_log=_log_suite)
                    test_case.save()
                except KeyError as e:
                    print('additional test case was added')
            _log_suite.passed_sum = suite_sum_passed
            _log_suite.passed = suite_passed
            _log_suite.tests_run = tests_run
            _log_suite.save(update_fields=["passed", "passed_sum", "tests_run"])
            return HttpResponse("success")
    return HttpResponse("failure")
