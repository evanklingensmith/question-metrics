from django.forms import ModelForm, ModelMultipleChoiceField, DateTimeInput, DateTimeField, Form
from .models import Student, Test, Question, TestEvent, Objective
from .plugins import updateObjectives, studentPackages
import datetime

class StudentForm(ModelForm):
    class Meta:
        model = Student
        fields = ['first_name', 'last_name']

class TestForm(ModelForm):
    class Meta:
        model = Test
        fields = ['name', 'description', 'questions']

    # def __init__(self, *args, **kwargs):
    #         super(TestForm, self).__init__(*args, **kwargs)
    #         self.fields['myfield'].widget.attrs.update({'class' : 'myfieldclass'})

class TestEventForm(ModelForm):
    class Meta:
        model = TestEvent
        fields = ['name', 'test', 'students']

    students = ModelMultipleChoiceField(queryset=Student.objects.all())

    def __init__(self, *args, **kwargs):
        if kwargs.get('instance'):
            initial = kwargs.setdefault('initial', {})
            initial['students'] = [t.pk for t in kwargs['instance'].students_set.all()]
            if len(kwargs['instance'].students_set.all()) < 1:
                raise ValueError('No students were set')
        ModelForm.__init__(self, *args, **kwargs)

    def save(self, commit=True):
        instance = ModelForm.save(self)
        if len(self.cleaned_data['students']) < 1:
            return -1
        instance.students.add(*self.cleaned_data['students'])
        instance.save()
        studentPackages.create(instance)
        return instance


class ObjectiveForm(ModelForm):
    class Meta:
        model = Objective
        fields = '__all__'
    
    questions = ModelMultipleChoiceField(queryset=Question.objects.all())

    def __init__(self, *args, **kwargs):
        if kwargs.get('instance'):
            initial = kwargs.setdefault('initial', {})
            initial['questions'] = [t.pk for t in kwargs['instance'].questions_set.all()]
        ModelForm.__init__(self, *args, **kwargs)

    def save(self, commit=True):
        instance = ModelForm.save(self)
        print(instance.id)
        instance.questions.add(*self.cleaned_data['questions'])
        instance.save()
        updateObjectives.addOrUpdateObjective(instance)
        return instance

